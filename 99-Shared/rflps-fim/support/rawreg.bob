<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>IOC Configurations</name>
  <width>1200</width>
  <height>1200</height>
  <scripts>
    <script file="EmbeddedPy">
      <text><![CDATA[# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

result_ = (PVUtil.getInt(pvs[0]) - PVUtil.getInt(pvs[1]))
#PVUtil.writePV(pvs[2], result_, 100)
if (result_ > 0):
	pvs[2].write(result_)
else:
	pvs[2].write(0)
]]></text>
      <pv_name>$(PREFIX):$(IOC_):ProcPeriod-RB</pv_name>
      <pv_name trigger="false">$(PREFIX):$(IOC_):LoopPeriod-RB</pv_name>
      <pv_name trigger="false">loc://netPeriod</pv_name>
    </script>
  </scripts>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_2</name>
    <class>TITLE-BAR</class>
    <x use_class="true">0</x>
    <y use_class="true">0</y>
    <width>1200</width>
    <height use_class="true">50</height>
    <line_width use_class="true">0</line_width>
    <background_color use_class="true">
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>TITLE</class>
    <text>RFLPS FIM - Read any Register</text>
    <x use_class="true">20</x>
    <y use_class="true">0</y>
    <width>650</width>
    <height use_class="true">50</height>
    <font use_class="true">
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <horizontal_alignment use_class="true">0</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_70</name>
    <text>Current FIM State</text>
    <x>870</x>
    <y>10</y>
    <width>140</width>
    <height>30</height>
    <font>
      <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update_43</name>
    <pv_name>$(PREFIX):$(IOC_):INTS-RB</pv_name>
    <x>1010</x>
    <y>10</y>
    <width>170</width>
    <height>30</height>
    <font>
      <font family="Liberation Sans" style="BOLD" size="16.0">
      </font>
    </font>
    <precision>0</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Enable Triggers from MTCA Backplane</name>
    <x>510</x>
    <y>70</y>
    <width>460</width>
    <height>106</height>
    <style>1</style>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </foreground_color>
    <background_color>
      <color name="Read_Background" red="230" green="235" blue="232">
      </color>
    </background_color>
    <transparent>true</transparent>
    <widget type="combo" version="2.0.0">
      <name>Boolean Button</name>
      <pv_name>$(PREFIX):$(IOC_):TriggerLine</pv_name>
      <x>32</x>
      <y>34</y>
      <width>150</width>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>LED</name>
      <pv_name>$(PREFIX):$(IOC_):TriggerLine-RB</pv_name>
      <x>242</x>
      <y>34</y>
      <width>170</width>
      <height>30</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_6</name>
      <text>Trigger Line Selection</text>
      <x>32</x>
      <y>11</y>
      <width>150</width>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_7</name>
      <text>Readback Value</text>
      <x>242</x>
      <y>11</y>
      <width>170</width>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>General Diagnostics</name>
    <x>30</x>
    <y>70</y>
    <width>460</width>
    <height>176</height>
    <style>1</style>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </foreground_color>
    <background_color>
      <color name="Read_Background" red="230" green="235" blue="232">
      </color>
    </background_color>
    <transparent>true</transparent>
    <widget type="label" version="2.0.0">
      <name>Label_8</name>
      <class>CAPTION</class>
      <text>Raw AI0</text>
      <x>19</x>
      <y>23</y>
      <width>70</width>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment use_class="true">2</horizontal_alignment>
      <vertical_alignment use_class="true">1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Entry_4</name>
      <pv_name>$(PREFIX):$(IOC_):RawAI0-RB</pv_name>
      <x>99</x>
      <y>23</y>
      <width>120</width>
      <background_color>
        <color name="Write_Background" red="224" green="217" blue="202">
        </color>
      </background_color>
      <wrap_words>false</wrap_words>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_9</name>
      <class>CAPTION</class>
      <text>Raw AI19</text>
      <x>229</x>
      <y>23</y>
      <width>70</width>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment use_class="true">2</horizontal_alignment>
      <vertical_alignment use_class="true">1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Entry_5</name>
      <pv_name>$(PREFIX):$(IOC_):RawAI19-RB</pv_name>
      <x>309</x>
      <y>23</y>
      <width>120</width>
      <background_color>
        <color name="Write_Background" red="224" green="217" blue="202">
        </color>
      </background_color>
      <wrap_words>false</wrap_words>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="byte_monitor" version="2.0.0">
      <name>Byte Monitor_3</name>
      <pv_name>$(PREFIX):$(IOC_):OUTSMLB-RB</pv_name>
      <x>32</x>
      <y>117</y>
      <width>100</width>
      <numBits>5</numBits>
    </widget>
    <widget type="byte_monitor" version="2.0.0">
      <name>Byte Monitor_4</name>
      <pv_name>$(PREFIX):$(IOC_):OUTSLSB-RB</pv_name>
      <x>132</x>
      <y>117</y>
      <width>307</width>
      <numBits>16</numBits>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_10</name>
      <text>20</text>
      <x>34</x>
      <y>98</y>
      <width>15</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_11</name>
      <text>0</text>
      <x>423</x>
      <y>98</y>
      <width>15</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_12</name>
      <text>1</text>
      <x>403</x>
      <y>98</y>
      <width>15</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_13</name>
      <text>2</text>
      <x>383</x>
      <y>98</y>
      <width>15</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_14</name>
      <text>3</text>
      <x>364</x>
      <y>98</y>
      <width>15</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_15</name>
      <text>4</text>
      <x>348</x>
      <y>98</y>
      <width>15</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_16</name>
      <text>5</text>
      <x>330</x>
      <y>98</y>
      <width>15</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_17</name>
      <text>6</text>
      <x>311</x>
      <y>98</y>
      <width>15</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_18</name>
      <text>7</text>
      <x>292</x>
      <y>98</y>
      <width>15</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_20</name>
      <class>CAPTION</class>
      <text>Digital Outputs Status</text>
      <x>19</x>
      <y>73</y>
      <width>155</width>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment use_class="true">2</horizontal_alignment>
      <vertical_alignment use_class="true">1</vertical_alignment>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Read Register</name>
    <x>30</x>
    <y>335</y>
    <width>1150</width>
    <height>235</height>
    <style>1</style>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </foreground_color>
    <background_color>
      <color name="Read_Background" red="230" green="235" blue="232">
      </color>
    </background_color>
    <transparent>true</transparent>
    <widget type="byte_monitor" version="2.0.0">
      <name>Byte Monitor_6</name>
      <pv_name>$(PREFIX):$(IOC_):RawReg-RB</pv_name>
      <x>9</x>
      <y>166</y>
      <width>1107</width>
      <height>34</height>
      <numBits>32</numBits>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_32</name>
      <text>0</text>
      <x>1089</x>
      <y>147</y>
      <width>18</width>
      <height>19</height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_33</name>
      <text>8</text>
      <x>819</x>
      <y>147</y>
      <width>18</width>
      <height>19</height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_34</name>
      <text>16</text>
      <x>539</x>
      <y>147</y>
      <width>18</width>
      <height>19</height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_35</name>
      <text>24</text>
      <x>259</x>
      <y>147</y>
      <width>18</width>
      <height>19</height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_36</name>
      <text>31</text>
      <x>19</x>
      <y>147</y>
      <width>18</width>
      <height>19</height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_37</name>
      <class>CAPTION</class>
      <text>Decimal</text>
      <x>249</x>
      <y>27</y>
      <width>70</width>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment use_class="true">2</horizontal_alignment>
      <vertical_alignment use_class="true">1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Entry_8</name>
      <pv_name>$(PREFIX):$(IOC_):SelectReg</pv_name>
      <x>329</x>
      <y>27</y>
      <width>120</width>
      <background_color>
        <color name="Write_Background" red="224" green="217" blue="202">
        </color>
      </background_color>
      <format>1</format>
      <precision>0</precision>
      <wrap_words>false</wrap_words>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_38</name>
      <class>CAPTION</class>
      <text>Hexa:</text>
      <x>37</x>
      <y>27</y>
      <width>70</width>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment use_class="true">2</horizontal_alignment>
      <vertical_alignment use_class="true">1</vertical_alignment>
    </widget>
    <widget type="textentry" version="3.0.0">
      <name>Text Entry_9</name>
      <pv_name>$(PREFIX):$(IOC_):SelectReg</pv_name>
      <x>119</x>
      <y>17</y>
      <width>110</width>
      <height>30</height>
      <format>4</format>
      <precision>0</precision>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_39</name>
      <class>CAPTION</class>
      <text>Reg. Value</text>
      <x>19</x>
      <y>117</y>
      <width>70</width>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment use_class="true">2</horizontal_alignment>
      <vertical_alignment use_class="true">1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Entry_10</name>
      <pv_name>$(PREFIX):$(IOC_):RawReg-RB</pv_name>
      <x>99</x>
      <y>117</y>
      <width>120</width>
      <background_color>
        <color name="Write_Background" red="224" green="217" blue="202">
        </color>
      </background_color>
      <format>1</format>
      <precision>0</precision>
      <wrap_words>false</wrap_words>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
  </widget>
</display>
