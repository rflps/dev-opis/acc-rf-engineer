# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil

trig_pv = PVUtil.getInt(pvs[0])
meas_pv = PVUtil.getInt(pvs[1])

devtrig_ = "$(AI00)"
devmeas_ = "$(AI00)"

#################################

if (trig_pv == 0):
	devtrig_ = "$(AI00)"

if (trig_pv == 1):
	devtrig_ = "$(AI01)"

if (trig_pv == 2):
	devtrig_ = "$(AI02)"

if (trig_pv == 3):
	devtrig_ = "$(AI03)"

if (trig_pv == 4):
	devtrig_ = "$(AI04)"

if (trig_pv == 5):
	devtrig_ = "$(AI05)"

if (trig_pv == 6):
	devtrig_ = "$(AI06)"

if (trig_pv == 7):
	devtrig_ = "$(AI07)"

if (trig_pv == 8):
	devtrig_ = "$(AI08)"

if (trig_pv == 9):
	devtrig_ = "$(AI09)"

if (trig_pv == 10):
	devtrig_ = "$(AI10)"

if (trig_pv == 11):
	devtrig_ = "$(AI11)"

if (trig_pv == 12):
	devtrig_ = "$(AI12)"

if (trig_pv == 13):
	devtrig_ = "$(AI13)"

if (trig_pv == 14):
	devtrig_ = "$(AI14)"

if (trig_pv == 15):
	devtrig_ = "$(AI15)"

if (trig_pv == 16):
	devtrig_ = "$(AI16)"

if (trig_pv == 17):
	devtrig_ = "$(AI17)"

if (trig_pv == 18):
	devtrig_ = "$(AI18)"

if (trig_pv == 19):
	devtrig_ = "$(AI19)"

#################################

if (meas_pv == 0):
	devmeas_ = "$(AI00)"

if (meas_pv == 1):
	devmeas_ = "$(AI01)"

if (meas_pv == 2):
	devmeas_ = "$(AI02)"

if (meas_pv == 3):
	devmeas_ = "$(AI03)"

if (meas_pv == 4):
	devmeas_ = "$(AI04)"

if (meas_pv == 5):
	devmeas_ = "$(AI05)"

if (meas_pv == 6):
	devmeas_ = "$(AI06)"

if (meas_pv == 7):
	devmeas_ = "$(AI07)"

if (meas_pv == 8):
	devmeas_ = "$(AI08)"

if (meas_pv == 9):
	devmeas_ = "$(AI09)"

if (meas_pv == 10):
	devmeas_ = "$(AI10)"

if (meas_pv == 11):
	devmeas_ = "$(AI11)"

if (meas_pv == 12):
	devmeas_ = "$(AI12)"

if (meas_pv == 13):
	devmeas_ = "$(AI13)"

if (meas_pv == 14):
	devmeas_ = "$(AI14)"

if (meas_pv == 15):
	devmeas_ = "$(AI15)"

if (meas_pv == 16):
	devmeas_ = "$(AI16)"

if (meas_pv == 17):
	devmeas_ = "$(AI17)"

if (meas_pv == 18):
	devmeas_ = "$(AI18)"

if (meas_pv == 19):
	devmeas_ = "$(AI19)"

#################################

widget.getPropertyValue("macros").add("DEVTRIG", devtrig_)
widget.getPropertyValue("macros").add("DEVMEAS", devmeas_)
widget.setPropertyValue("file", "RefPwr_XYGraph.bob")

