from org.csstudio.display.builder.runtime.script import PVUtil
import os
import subprocess
from os.path import expanduser
from time import sleep
import math

save_cal_table = PVUtil.getInt(pvs[13])

if save_cal_table == 1:
    SCBmeas             = PVUtil.getDoubleArray(pvs[0])
    LLRFmeas            = PVUtil.getDoubleArray(pvs[1])
    PWMTmeas            = PVUtil.getDoubleArray(pvs[2])
    cal_llrf            = PVUtil.getInt(pvs[3])           # cal_llrf
    cal_scb             = PVUtil.getInt(pvs[4])           # cal_scb
    SCBfilename         = PVUtil.getString(pvs[5])        # loc://SCB_filename
    LLRFfilename        = PVUtil.getString(pvs[6])        # loc://LLRF_filename
    KW                  = PVUtil.getDouble(pvs[7])        # loc://KW
    DC_coupling         = PVUtil.getDouble(pvs[8])
    cable_att           = PVUtil.getDouble(pvs[9])
    cable_number        = PVUtil.getString(pvs[10])
    filter_att          = PVUtil.getDouble(pvs[11])
    system_name         = PVUtil.getString(pvs[12])

    raw_SCBfilename  = SCBfilename + "_raw.csv"
    raw_LLRFfilename = LLRFfilename + "_raw.csv"
## make the directories
    try:
        os.mkdir("../Measurements/"+ system_name)
        os.mkdir("../Measurements/"+ system_name + "/SCB")
        os.mkdir("../Measurements/"+ system_name + "/LLRF")
    except:
        temp = PVUtil.createPV("loc://message",5000)
        temp.write('Directories already exist.')
        PVUtil.releasePV(temp)

    if cal_llrf == 1:
        llrffile = open("../Measurements/"+ system_name + "/LLRF/" +raw_LLRFfilename,"w")
##        if KW == 1:
##            llrffile.write("# KWatt" + ',' + " raw" +'\n')
##        else:
##            llrffile.write("# Watt" + ',' + " raw" +'\n')
        for cnt in range(len(LLRFmeas)):
            llrffile.write(str(PWMTmeas[cnt]) + ',' + str(LLRFmeas[cnt]) +'\n')
##        llrffile.write('# \n')
##        llrffile.write('# \n')
##        llrffile.write('# \n')
##        llrffile.write('# \n')
##        llrffile.write('# -----------------------------------------------------------\n')
##        llrffile.write("# Wave-guide Directional coupler attenuation is " + str(DC_coupling) + "dB" +'\n')
##        llrffile.write("# Cable number from Wave-guide Directional coupler to patch panel  is " + cable_number +'\n')
##        llrffile.write("# Cable attenuation is " + str(cable_att) + "dB" +'\n')
##        llrffile.write("# Filter and directional coupler attenuation is " + str(filter_att) + "dB" +'\n')
##        llrffile.write('# -----------------------------------------------------------\n')
        llrffile.close()
    
    if cal_scb == 1:
        scbfile = open("../Measurements/"+ system_name + "/SCB/" +raw_SCBfilename,"w")
##        if KW == 1:
##            scbfile.write("# KWatt" + ',' + " raw" +'\n')
##        else:
##            scbfile.write("# Watt" + ',' + " raw" +'\n')
        for cnt in range(len(SCBmeas)):
            scbfile.write(str(PWMTmeas[cnt]) + ',' + str(SCBmeas[cnt]) +'\n')
##        scbfile.write('# \n')
##        scbfile.write('# \n')
##        scbfile.write('# \n')
##        scbfile.write('# \n')
##        scbfile.write('# -----------------------------------------------------------\n')
##        scbfile.write("# Wave-guide Directional coupler attenuation is " + str(DC_coupling) + "dB" +'\n')
##        scbfile.write("# Cable number from Wave-guide Directional coupler to patch panel  is " + cable_number +'\n')
##        scbfile.write("# Cable attenuation is " + str(cable_att) + "dB" +'\n')
##        scbfile.write("# Filter and directional coupler attenuation is " + str(filter_att) + "dB" +'\n')
##        scbfile.write('# -----------------------------------------------------------\n')
        scbfile.close()

    temp = PVUtil.createPV("loc://message",5000)
    temp.write('Raw calibration files saved.')
    PVUtil.releasePV(temp)
